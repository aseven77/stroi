$('.team-slider').each(function() {
    var swiper2 = new Swiper( this, {
       slidesPerView: 'auto',
        spaceBetween: 10,
        breakpoints: {
            640: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 10,
            },
            1025: {
              slidesPerView: 3,
              spaceBetween: 30,
            },
            1150: {
              slidesPerView: 4,
              spaceBetween: 30,
            },            
            
        },
        fadeEffect: {
            crossFade: true
        },      
        navigation: {
            nextEl: $(this).parent().find('.swiper-button-next') ,
            prevEl: $(this).parent().find('.swiper-button-prev')
        },
    });
});