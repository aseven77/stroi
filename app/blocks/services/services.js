$(".services-arrow").on("click" , function() {
    let th = $(this).closest(".services")
    th.addClass("opened");
    $(this).hide(300)
})
$(".js-services-show-all").on("click", function (event) {
    event.preventDefault()
    
    $(this).toggleClass("openeds");
    if($(this).hasClass("openeds")) {
        $(".services-ls").addClass("show-services");
        $(this).find("span").text("Свернуть");
        
    }
    else {
        $(".services-ls").removeClass("show-services");
        $(this).find("span").text("Показать еще");
    }
})