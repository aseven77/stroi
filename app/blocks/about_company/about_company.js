$(".js-number").on('keyup', function() {
    $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''))
});
$("select").on("change" , function () {
    $(this).closest("label.select").next("label.select").removeClass("disabled")
});




$("#option-1").on("change" , function () {
    
    $("label[for=option-1]").removeClass("error")
});
$("#option-2").on("change" , function () {
    $(".js-number").prop('disabled', false)
});
$("#option-2").on("click" , function () {
    if($(this).filter("disabled")) {
        $("label[for=option-1]").addClass("error")
    }
});


if(innerWidth > 767) {
    $('select').selectpicker();

    $(".js-select-letter").on("click" , function() {
        $(".js-select__letter").selectpicker('val' , $('input[name=letter]:checked').attr("id"));
        $("#modal-district").modal("hide")
    });
    $('.js-select__letter').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
         $("#modal-district").find("input[id="+$(this).val()+"]").attr('checked', 'checked')
        return false
    });
    

    $(".js-select-services").on("click" , function() {
        var list = [];
        $('input[name=services]:checked').each(function() {
            list.push($(this).attr('id'))
        })
       $(".js-select__services").selectpicker('val' , list);
        $("#modal-services").modal("hide")
    });
    $('.js-select__services').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var arr = $(this).val()
        arr.map(function(value){
            $("#modal-services").find("input[id="+value+"]").attr('checked', 'checked')
        })
        return false
   });
}

$("#modal-services").find("input[type=checkbox]").on("change" , function() {
    if($(this).closest(".labes").find(".labes-list").length == 1) {

        if($(this).is(':checked')) {
            $(this).closest(".labes").find(".labes-list input").prop('checked', true);

        } else {
            $(this).closest(".labes").find(".labes-list input").prop('checked', false);
        }

   }

})
$(".js-arrow").on("click" , function() {
    $(this).next(".labes-list").slideToggle();
    $(this).toggleClass("active")
})
$("form[name=js-calculate]").on("submit" , function(event) {
    event.preventDefault();
    if($(this).find("[name=option-1]").val() == null) {
        $("label[for=option-1]").addClass("error")
    }
    else {
        $("label[for=option-1]").removeClass("error")
    }
    if($(this).find("[name=option-2]").val() == null) {
        $("label[for=option-2]").addClass("error")
    }
    else {
        $("label[for=option-2]").removeClass("error")
    }    
    if(!$(".js-number").val().length) {
        $("label[for=number]").addClass("error")
    }
    if($(".js-number").val().length) {
        $("#modal-calculate").find(".js-option-1").text($(this).find("[name=option-1]").val());
        $("#modal-calculate").find(".js-option-2").text($(this).find("[name=option-2]").val());
        $("#modal-calculate").find(".js-option-3").text($(this).find(".js-number").val());
        $("#modal-calculate").modal("show")
    }
})
$(".js-modal-callback").on("click" , function(event) {
    event.preventDefault();
    $("#modal-calculate").modal("hide")
    setTimeout(function() {
        $("#modal-callback").modal("show")
    },500)

})