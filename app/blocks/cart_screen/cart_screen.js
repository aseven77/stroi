$(".js-open-video").on("click", function(event) {
    event.preventDefault();
    var src = $(this).data("src");
    var lnk = src.match(/=\w(.*)/)[0].slice(1);
    $('#video-youtube').attr("src", "https://www.youtube.com/embed/" + lnk + "?enablejsapi=1&rel=0&amp;amp;showinfo=0")
    setTimeout(function() {
        document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
    }, 1000);
});
$(".js-video-close").on("click" , function() {
    $("#modalVideo").modal("hide");
    $("#video-youtube").attr("src", "");
    document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
});
$(".js-scroll").on("click" , function(event) {
    event.preventDefault();
    var el = $(this);
    var dest = el.attr("href");
    if(dest !== undefined && dest !== "") {
        $("html").animate({ 
            scrollTop: $(dest).offset().top 
        }, 500);
    }
});
$(".js-add-favorites").on("click" , function() {
    
    if($(".control--favorites").hasClass("control--favorites--active")) {
        $(this).closest(".control--favorites").removeClass("control--favorites--active");
        $(this).closest(".control--favorites").find(".control__main").text("Добавить в Избранное");
        return false;
    }
    $(this).closest(".control--favorites").addClass("control--favorites--active");
    $(this).closest(".control--favorites").find(".control__main").text("Удалить из Избранного");
});
$(".js-control-share").on("click" , function() {
    $(this).addClass("show-share")
});
$(document).mouseup(function(e) { 
    var share = $(".js-control-share");
    if (share.has(e.target).length === 0) {
        share.removeClass("show-share")
    }
});
$(".js-copylink").on("click" , function(event){
    event.preventDefault();
    var hr = window.location.href;
    copyTextToClipboard(hr);
    return false
});

function copyTextToClipboard(text) {
    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            // console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }

        document.body.removeChild(textArea);
    };

    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function() {
        // console.log('Async: Copying to clipboard was successful!');
    }, function(err) {
        console.error('Async: Could not copy text: ', err);
    });
}