$('.partners-list').each(function() {
    var swiper = new Swiper( this, {
        
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },       
        slidesPerView: 2,
        spaceBetween: 0,
        breakpoints: {
            640: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 4,
              spaceBetween: 10,
            },

            1150: {
              slidesPerView: 6,
              spaceBetween: 30,
            },            
            
        },        
        navigation: {
            nextEl: $(this).parent().find('.js-btns.swiper-button-next') ,
            prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
        },
    });
});