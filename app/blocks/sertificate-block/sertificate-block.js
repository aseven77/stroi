$('.sertificate-slider').each(function() {
  var swipersertificate = new Swiper( this, {
      slidesPerView: 'auto',
      spaceBetween: 10,
      slidesPerView: 2,
      breakpoints: {
          540: {
            slidesPerView: 2,
            spaceBetween: 10,
          },
          640: {
            slidesPerView: 3,
            spaceBetween: 10,
          },
          1000: {
            slidesPerView: 4,
            spaceBetween: 10,
          },
          1150: {
            slidesPerView: 5,
            spaceBetween: 30,
          }          
          
      },
      fadeEffect: {
          crossFade: true
      },      
      navigation: {
          nextEl: $(this).parent().find('.swiper-button-next') ,
          prevEl: $(this).parent().find('.swiper-button-prev')
      },
  });
});