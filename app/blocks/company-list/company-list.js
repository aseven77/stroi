$('.company-list').each(function() {
    var swiper = new Swiper( this, {
      slidesPerView: 'auto',
      spaceBetween: 30,
      fadeEffect: {
        crossFade: true
      },
      navigation: {
          nextEl: $(this).parent().find('.js-btns.swiper-button-next') ,
          prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
      },
    });
});