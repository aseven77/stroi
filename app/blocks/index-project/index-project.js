$('.object-slider').each(function() {
    var swiper2 = new Swiper( this, {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,

        pagination: {
            el: '.swiper-pagination',
            type: 'fraction'
        },     
       
        navigation: {
            nextEl: $(this).closest(this).find('.js-slider-img.swiper-button-next') ,
            prevEl: $(this).closest(this).find('.js-slider-img.swiper-button-prev')
        },
    });
});