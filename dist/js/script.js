"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {
  $("input[type=tel]").inputmask({
    mask: "+7 999 999 99 99",
    placeholder: "",
    showMaskOnHover: false
  });
  $(".js-number").on('keyup', function () {
    $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''));
  });
  $("select").on("change", function () {
    $(this).closest("label.select").next("label.select").removeClass("disabled");
  });
  $("#option-1").on("change", function () {
    $("label[for=option-1]").removeClass("error");
  });
  $("#option-2").on("change", function () {
    $(".js-number").prop('disabled', false);
  });
  $("#option-2").on("click", function () {
    if ($(this).filter("disabled")) {
      $("label[for=option-1]").addClass("error");
    }
  });

  if (innerWidth > 767) {
    $('select').selectpicker();
    $(".js-select-letter").on("click", function () {
      $(".js-select__letter").selectpicker('val', $('input[name=letter]:checked').attr("id"));
      $("#modal-district").modal("hide");
    });
    $('.js-select__letter').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
      $("#modal-district").find("input[id=" + $(this).val() + "]").attr('checked', 'checked');
      return false;
    });
    $(".js-select-services").on("click", function () {
      var list = [];
      $('input[name=services]:checked').each(function () {
        list.push($(this).attr('id'));
      });
      $(".js-select__services").selectpicker('val', list);
      $("#modal-services").modal("hide");
    });
    $('.js-select__services').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
      var arr = $(this).val();
      arr.map(function (value) {
        $("#modal-services").find("input[id=" + value + "]").attr('checked', 'checked');
      });
      return false;
    });
  }

  $("#modal-services").find("input[type=checkbox]").on("change", function () {
    if ($(this).closest(".labes").find(".labes-list").length == 1) {
      if ($(this).is(':checked')) {
        $(this).closest(".labes").find(".labes-list input").prop('checked', true);
      } else {
        $(this).closest(".labes").find(".labes-list input").prop('checked', false);
      }
    }
  });
  $(".js-arrow").on("click", function () {
    $(this).next(".labes-list").slideToggle();
    $(this).toggleClass("active");
  });
  $("form[name=js-calculate]").on("submit", function (event) {
    event.preventDefault();

    if ($(this).find("[name=option-1]").val() == null) {
      $("label[for=option-1]").addClass("error");
    } else {
      $("label[for=option-1]").removeClass("error");
    }

    if ($(this).find("[name=option-2]").val() == null) {
      $("label[for=option-2]").addClass("error");
    } else {
      $("label[for=option-2]").removeClass("error");
    }

    if (!$(".js-number").val().length) {
      $("label[for=number]").addClass("error");
    }

    if ($(".js-number").val().length) {
      $("#modal-calculate").find(".js-option-1").text($(this).find("[name=option-1]").val());
      $("#modal-calculate").find(".js-option-2").text($(this).find("[name=option-2]").val());
      $("#modal-calculate").find(".js-option-3").text($(this).find(".js-number").val());
      $("#modal-calculate").modal("show");
    }
  });
  $(".js-modal-callback").on("click", function (event) {
    event.preventDefault();
    $("#modal-calculate").modal("hide");
    setTimeout(function () {
      $("#modal-callback").modal("show");
    }, 500);
  });
  $(".js-open-video").on("click", function (event) {
    event.preventDefault();
    var src = $(this).data("src");
    var lnk = src.match(/=\w(.*)/)[0].slice(1);
    $('#video-youtube').attr("src", "https://www.youtube.com/embed/" + lnk + "?enablejsapi=1&rel=0&amp;amp;showinfo=0");
    setTimeout(function () {
      document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
    }, 1000);
  });
  $(".js-video-close").on("click", function () {
    $("#modalVideo").modal("hide");
    $("#video-youtube").attr("src", "");
    document.getElementById('video-youtube').contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
  });
  $(".js-scroll").on("click", function (event) {
    event.preventDefault();
    var el = $(this);
    var dest = el.attr("href");

    if (dest !== undefined && dest !== "") {
      $("html").animate({
        scrollTop: $(dest).offset().top
      }, 500);
    }
  });
  $(".js-add-favorites").on("click", function () {
    if ($(".control--favorites").hasClass("control--favorites--active")) {
      $(this).closest(".control--favorites").removeClass("control--favorites--active");
      $(this).closest(".control--favorites").find(".control__main").text("Добавить в Избранное");
      return false;
    }

    $(this).closest(".control--favorites").addClass("control--favorites--active");
    $(this).closest(".control--favorites").find(".control__main").text("Удалить из Избранного");
  });
  $(".js-control-share").on("click", function () {
    $(this).addClass("show-share");
  });
  $(document).mouseup(function (e) {
    var share = $(".js-control-share");

    if (share.has(e.target).length === 0) {
      share.removeClass("show-share");
    }
  });
  $(".js-copylink").on("click", function (event) {
    event.preventDefault();
    var hr = window.location.href;
    copyTextToClipboard(hr);
    return false;
  });

  function copyTextToClipboard(text) {
    function fallbackCopyTextToClipboard(text) {
      var textArea = document.createElement("textarea");
      textArea.value = text;
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful'; // console.log('Fallback: Copying text command was ' + msg);
      } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
      }

      document.body.removeChild(textArea);
    }

    ;

    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }

    navigator.clipboard.writeText(text).then(function () {// console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
      console.error('Async: Could not copy text: ', err);
    });
  }

  $('.company-list').each(function () {
    var swiper = new Swiper(this, {
      slidesPerView: 'auto',
      spaceBetween: 30,
      fadeEffect: {
        crossFade: true
      },
      navigation: {
        nextEl: $(this).parent().find('.js-btns.swiper-button-next'),
        prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
      }
    });
  });
  $(".js-open-search").on("click", function () {
    $(".search-form").addClass("search-form--active");
  });
  $(".js-open-menu").on("click", function () {
    $("body").addClass("active-menu");
  });
  $(".menu-close").on("click", function () {
    $("body").removeClass("active-menu");
  });
  $('.object-slider').each(function () {
    var swiper2 = new Swiper(this, {
      slidesPerView: 1,
      spaceBetween: 0,
      centeredSlides: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
      },
      navigation: {
        nextEl: $(this).closest(this).find('.js-slider-img.swiper-button-next'),
        prevEl: $(this).closest(this).find('.js-slider-img.swiper-button-prev')
      }
    });
  });
  $('.partners-list').each(function () {
    var swiper = new Swiper(this, {
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
      },
      slidesPerView: 2,
      spaceBetween: 0,
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 10
        },
        1150: {
          slidesPerView: 6,
          spaceBetween: 30
        }
      },
      navigation: {
        nextEl: $(this).parent().find('.js-btns.swiper-button-next'),
        prevEl: $(this).parent().find('.js-btns.swiper-button-prev')
      }
    });
  });
  $(".js-alltext").on("click", function (event) {
    event.preventDefault();
    var th = $(this).closest(".body-text");
    th.find(".small-text").hide();
    th.find(".all-text").show();
  });
  $('.sertificate-slider').each(function () {
    var _ref;

    var swipersertificate = new Swiper(this, (_ref = {
      slidesPerView: 'auto',
      spaceBetween: 10
    }, _defineProperty(_ref, "slidesPerView", 2), _defineProperty(_ref, "breakpoints", {
      540: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      1000: {
        slidesPerView: 4,
        spaceBetween: 10
      },
      1150: {
        slidesPerView: 5,
        spaceBetween: 30
      }
    }), _defineProperty(_ref, "fadeEffect", {
      crossFade: true
    }), _defineProperty(_ref, "navigation", {
      nextEl: $(this).parent().find('.swiper-button-next'),
      prevEl: $(this).parent().find('.swiper-button-prev')
    }), _ref));
  });
  $(".services-arrow").on("click", function () {
    var th = $(this).closest(".services");
    th.addClass("opened");
    $(this).hide(300);
  });
  $(".js-services-show-all").on("click", function (event) {
    event.preventDefault();
    $(this).toggleClass("openeds");

    if ($(this).hasClass("openeds")) {
      $(".services-ls").addClass("show-services");
      $(this).find("span").text("Свернуть");
    } else {
      $(".services-ls").removeClass("show-services");
      $(this).find("span").text("Показать еще");
    }
  });
  $('.team-slider').each(function () {
    var swiper2 = new Swiper(this, {
      slidesPerView: 'auto',
      spaceBetween: 10,
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10
        },
        1025: {
          slidesPerView: 3,
          spaceBetween: 30
        },
        1150: {
          slidesPerView: 4,
          spaceBetween: 30
        }
      },
      fadeEffect: {
        crossFade: true
      },
      navigation: {
        nextEl: $(this).parent().find('.swiper-button-next'),
        prevEl: $(this).parent().find('.swiper-button-prev')
      }
    });
  });
});